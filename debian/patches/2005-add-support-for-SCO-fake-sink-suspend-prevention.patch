From: Ratchanan Srirattanamet <ratchanan@ubports.com>
Date: Tue, 19 May 2020 22:52:00 +0700
Subject: [PATCH] add support for SCO fake sink suspend prevention

This commit brings the code from Canonical which make the sink,
depending on the output selected, prevent the suspension of the
Bluetooth headset. This is forward-ported from the old version using
the code from [1].

For this to work properly, it also requires a custom patch in the
Pulseaudio's bluez module.

[1]
  - https://github.com/saemy/pulseaudio/commit/4bb6142da20b7f38c2a343a4f007a3c0c2787b97
  - https://github.com/saemy/pulseaudio/commit/60d26f12dc67af7f634aa4e5f361e7562ec050b1

[PATCH] sink: move fake SCO property setting to a hook instead

Pulseaudio calls the sink's set_port callback on IO thread instead of
main thread if "deferred volumes" are in use, and with 65483884 ("sink:
Enable deferred volume for hw volume control"), we're doing just that.
This means we cannot update SCO fake sink's property from that callback,
as an assert will cause the whole PA to crash.

Thus, move the property setting logic into a hook, which is guaranteed
to run on the main thread. Yes, I know that it's a little bit silly to
run hook for our own sink, but that's the way it has to be.

Note that we register a separated hook from the existing `sink_port_
changed_hook_cb`, since that one is used for another purpose and has
some hook/unhook logic which I would rather not touch.

Version 2:
  - Account for the fact that `data->device_port` can be null, presumbly
    when switching to a parking port.

Fixes: https://github.com/ubports/ubuntu-touch/issues/1977
---
 src/droid/droid-sink.c        | 87 +++++++++++++++++++++++++++++++++++++++++++
 src/droid/module-droid-sink.c |  4 +-
 2 files changed, 90 insertions(+), 1 deletion(-)

diff --git a/src/droid/droid-sink.c b/src/droid/droid-sink.c
index 84850ef..332c0af 100644
--- a/src/droid/droid-sink.c
+++ b/src/droid/droid-sink.c
@@ -102,6 +102,10 @@ struct userdata {
     pa_droid_card_data *card_data;
     pa_droid_hw_module *hw_module;
     pa_droid_stream *stream;
+
+    char *sco_fake_sink_name;
+    struct pa_sink *sco_fake_sink;
+    pa_hook_slot *sink_port_changed_hook_slot_for_scofakesink;
 };
 
 #define DEFAULT_MODULE_ID "primary"
@@ -123,11 +127,16 @@ typedef struct droid_parameter_mapping {
 #define DEFAULT_VOICE_CONTROL_PROPERTY_KEY      "media.role"
 #define DEFAULT_VOICE_CONTROL_PROPERTY_VALUE    "phone"
 
+/* Name of the fake sco sink used for HSP (used to set transport property) */
+#define DEFAULT_SCO_FAKE_SINK "sink.fake.sco"
+#define HSP_PREVENT_SUSPEND_STR "bluetooth.hsp.prevent.suspend.transport"
+
 static void parameter_free(droid_parameter_mapping *m);
 static void userdata_free(struct userdata *u);
 static void set_voice_volume(struct userdata *u, pa_sink_input *i);
 static void apply_volume(pa_sink *s);
 static pa_sink_input *find_volume_control_sink_input(struct userdata *u);
+static struct pa_sink *pa_sco_fake_sink_discover(pa_core *core, const char *sink_name);
 
 static bool add_extra_devices(struct userdata *u, audio_devices_t device) {
     dm_list_entry *prev;
@@ -193,6 +202,19 @@ static void clear_extra_devices(struct userdata *u) {
     u->override_device_port = NULL;
 }
 
+static void set_fake_sco_sink_transport_property(struct userdata *u, const char *value) {
+    pa_proplist *pl;
+
+    pa_assert(u);
+    pa_assert(value);
+    pa_assert(u->sco_fake_sink);
+
+    pl = pa_proplist_new();
+    pa_proplist_sets(pl, HSP_PREVENT_SUSPEND_STR, value);
+    pa_sink_update_proplist(u->sco_fake_sink, PA_UPDATE_REPLACE, pl);
+    pa_proplist_free(pl);
+}
+
 /* Called from main context during voice calls, and from IO context during media operation. */
 static void do_routing(struct userdata *u) {
     dm_config_port *routing = NULL;
@@ -521,6 +543,41 @@ static int sink_set_port_cb(pa_sink *s, pa_device_port *p) {
     return 0;
 }
 
+/* Done as a hook instead of in the above function since it runs on the IO thread,
+ * and proplist update needs to happen on the main thread. */
+static pa_hook_result_t sink_port_changed_hook_for_scofakesink_cb(pa_core *c, pa_sink *sink, struct userdata *u) {
+    pa_device_port *port;
+    pa_droid_port_data *data;
+    const char *sco_transport_enabled;
+
+    if (sink != u->sink)
+        return PA_HOOK_OK;
+
+    port = sink->active_port;
+    data = PA_DEVICE_PORT_DATA(port);
+
+    if (!data->device_port) {
+        /* Do nothing for parking port. */
+        return PA_HOOK_OK;
+    }
+
+    /* See if the sco fake sink element is available (only when needed) */
+    if ((u->sco_fake_sink == NULL) && (data->device_port->type & AUDIO_DEVICE_OUT_ALL_SCO))
+        u->sco_fake_sink = pa_sco_fake_sink_discover(u->core, u->sco_fake_sink_name);
+
+    /* Update the bluetooth hsp transport property before we do the routing */
+    if (u->sco_fake_sink) {
+        sco_transport_enabled = pa_proplist_gets(u->sco_fake_sink->proplist, HSP_PREVENT_SUSPEND_STR);
+        if (sco_transport_enabled && pa_streq(sco_transport_enabled, "true")) {
+            if (data->device_port->type & ~AUDIO_DEVICE_OUT_ALL_SCO)
+                set_fake_sco_sink_transport_property(u, "false");
+        } else if (data->device_port->type & AUDIO_DEVICE_OUT_ALL_SCO)
+            set_fake_sco_sink_transport_property(u, "true");
+    }
+
+    return PA_HOOK_OK;
+}
+
 static void apply_volume(pa_sink *s) {
     struct userdata *u = s->userdata;
     pa_cvolume r;
@@ -928,6 +985,25 @@ static pa_hook_result_t sink_proplist_changed_hook_cb(pa_core *c, pa_sink *sink,
     return PA_HOOK_OK;
 }
 
+static struct pa_sink *pa_sco_fake_sink_discover(pa_core *core, const char *sink_name) {
+    struct pa_sink *sink;
+    pa_idxset *idxset;
+    void *state = NULL;
+
+    pa_assert(core);
+    pa_assert(sink_name);
+    pa_assert_se((idxset = core->sinks));
+
+    while ((sink = pa_idxset_iterate(idxset, &state, NULL)) != NULL) {
+        if (pa_streq(sink_name, sink->name)) {
+            pa_log_debug("Found fake SCO sink '%s'", sink_name);
+            return sink;
+        }
+    }
+
+    return NULL;
+}
+
 pa_sink *pa_droid_sink_new(pa_module *m,
                              pa_modargs *ma,
                              const char *driver,
@@ -1045,6 +1121,7 @@ pa_sink *pa_droid_sink_new(pa_module *m,
     u->voice_virtual_stream = voice_virtual_stream;
     u->voice_property_key   = pa_xstrdup(pa_modargs_get_value(ma, "voice_property_key", DEFAULT_VOICE_CONTROL_PROPERTY_KEY));
     u->voice_property_value = pa_xstrdup(pa_modargs_get_value(ma, "voice_property_value", DEFAULT_VOICE_CONTROL_PROPERTY_VALUE));
+    u->sco_fake_sink_name = pa_xstrdup(pa_modargs_get_value(ma, "sco_fake_sink", DEFAULT_SCO_FAKE_SINK));
     u->extra_devices_stack = dm_list_new();
 
     if (card_data) {
@@ -1188,6 +1265,10 @@ pa_sink *pa_droid_sink_new(pa_module *m,
         u->sink->set_port = sink_set_port_cb;
     }
 
+    /* Hooks for setting fake-SCO properties. */
+    u->sink_port_changed_hook_slot_for_scofakesink = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SINK_PORT_CHANGED], PA_HOOK_LATE,
+            (pa_hook_cb_t) sink_port_changed_hook_for_scofakesink_cb, u);
+
     update_volumes(u);
 
     pa_droid_stream_suspend(u->stream, false);
@@ -1249,6 +1330,9 @@ static void userdata_free(struct userdata *u) {
     if (u->sink_proplist_changed_hook_slot)
         pa_hook_slot_free(u->sink_proplist_changed_hook_slot);
 
+    if (u->sink_port_changed_hook_slot_for_scofakesink)
+        pa_hook_slot_free(u->sink_port_changed_hook_slot_for_scofakesink);
+
     if (u->sink)
         pa_sink_unref(u->sink);
 
@@ -1267,6 +1351,9 @@ static void userdata_free(struct userdata *u) {
     if (u->hw_module)
         pa_droid_hw_module_unref(u->hw_module);
 
+    if (u->sco_fake_sink_name)
+        pa_xfree(u->sco_fake_sink_name);
+
     if (u->voice_property_key)
         pa_xfree(u->voice_property_key);
     if (u->voice_property_value)
diff --git a/src/droid/module-droid-sink.c b/src/droid/module-droid-sink.c
index 13546cd..8881d16 100644
--- a/src/droid/module-droid-sink.c
+++ b/src/droid/module-droid-sink.c
@@ -44,7 +44,8 @@
 PA_MODULE_AUTHOR("Juho Hämäläinen");
 PA_MODULE_DESCRIPTION("Droid sink");
 PA_MODULE_USAGE("master_sink=<sink to connect to> "
-                "sink_name=<name of created sink>");
+                "sink_name=<name of created sink> "
+                "sco_fake_sink=<name of the fake sco sink used for hsp>");
 PA_MODULE_VERSION(PACKAGE_VERSION);
 
 static const char* const valid_modargs[] = {
@@ -70,6 +71,7 @@ static const char* const valid_modargs[] = {
     "voice_property_key",
     "voice_property_value",
     "voice_virtual_stream",
+    "sco_fake_sink",
     NULL,
 };
 
