From: Ratchanan Srirattanamet <ratchanan@ubports.com>
Date: Fri, 22 May 2020 23:34:56 +0700
Subject: [PATCH] Support adding suffix to module name & specify android
 headers pkg name

This allows distributions to compile & ship multiple versions of the
module in the same rootfs, choosing between them at runtime.

[ratchanan@ubports.com: forward-port to Meson build system]
---
 meson.build                    |  8 +++++++-
 meson_options.txt              |  9 +++++++++
 src/common/libdroid-util.pc.in |  4 ++--
 src/common/meson.build         |  8 +++++---
 src/droid/meson.build          | 18 ++++++++++--------
 5 files changed, 33 insertions(+), 14 deletions(-)

diff --git a/meson.build b/meson.build
index 89a490b..4a4c498 100644
--- a/meson.build
+++ b/meson.build
@@ -16,7 +16,7 @@ pa_c_args += ['-DDROID_DEVICE_@0@=1'.format(droiddevice.underscorify().to_upper(
 pa_c_args += ['-DDROID_DEVICE_STRING="@0@"'.format(droiddevice)]
 
 # dependencies
-droid_headers_dep = dependency('android-headers', required : true)
+droid_headers_dep = dependency(get_option('android-headers'), required : true)
 expat_dep = dependency('expat', version : '>= 2.1', required : true)
 hybris_dep = dependency('libhardware', version : '>= 0.1.0', required : true)
 hybris_common_dep = cc.find_library('hybris-common', required : true)
@@ -41,6 +41,12 @@ endif
 privlibdir = join_paths(libdir, 'pulseaudio')
 rpath_dirs = join_paths(privlibdir) + ':' + join_paths(modlibexecdir)
 
+if get_option('module-suffix') == ''
+  droid_module_suffix = ''
+else
+  droid_module_suffix = '-' + get_option('module-suffix')
+endif
+
 cdata = configuration_data()
 cdata.set_quoted('PACKAGE', meson.project_name())
 cdata.set_quoted('PACKAGE_NAME', meson.project_name())
diff --git a/meson_options.txt b/meson_options.txt
index a0484e3..560b48a 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -13,3 +13,12 @@ option('droid-device',
 option('modlibexecdir',
        type : 'string',
        description : 'Specify location where modules will be installed')
+
+option('android-headers',
+       type : 'string',
+       value : 'android-headers',
+       description : 'Specify custom android headers package')
+option('module-suffix',
+       type : 'string',
+       value : '',
+       description : 'Specify suffixes to all modules and libraries. "-" will be included automatically')
diff --git a/src/common/libdroid-util.pc.in b/src/common/libdroid-util.pc.in
index 523e4b0..d52cc78 100644
--- a/src/common/libdroid-util.pc.in
+++ b/src/common/libdroid-util.pc.in
@@ -4,8 +4,8 @@ libdir=@libdir@
 includedir=${prefix}/include
 libexecdir=@libexecdir@
 
-Name: libdroid-util
+Name: libdroid-util@DROID_MODULE_SUFFIX@
 Description: Common droid module building interface.
 Version: @PA_MODULE_VERSION@
-Libs: -L${libdir}/pulse-@PA_MAJORMINOR@/modules -ldroid-util
+Libs: -L${libdir}/pulse-@PA_MAJORMINOR@/modules -ldroid-util@DROID_MODULE_SUFFIX@
 Cflags: -D_REENTRANT -I${includedir}/pulsecore/modules
diff --git a/src/common/meson.build b/src/common/meson.build
index 9ab9abf..05298fb 100644
--- a/src/common/meson.build
+++ b/src/common/meson.build
@@ -27,9 +27,10 @@ libdroid_util_deps = [
   pulsecore_dep,
 ]
 
-install_headers(libdroid_util_headers, subdir : 'pulsecore/modules/droid')
+install_headers(libdroid_util_headers,
+                subdir : 'pulsecore/modules/droid' + droid_module_suffix)
 
-libdroid_util = library('droid-util',
+libdroid_util = library('droid-util' + droid_module_suffix,
   libdroid_util_sources,
   c_args : [pa_c_args, '-DPULSEAUDIO_VERSION=@0@'.format(pa_version_major)],
   dependencies : libdroid_util_deps,
@@ -55,10 +56,11 @@ pc_cdata.set('libdir', libdir)
 pc_cdata.set('libexecdir', get_option('libexecdir'))
 pc_cdata.set('PA_MAJORMINOR', pa_version_major_minor)
 pc_cdata.set('PA_MODULE_VERSION', pa_version_module)
+pc_cdata.set('DROID_MODULE_SUFFIX', droid_module_suffix)
 
 configure_file(
   input : 'libdroid-util.pc.in',
-  output : 'libdroid-util.pc',
+  output : 'libdroid-util' + droid_module_suffix + '.pc',
   configuration : pc_cdata,
   install_dir : join_paths(libdir, 'pkgconfig')
 )
diff --git a/src/droid/meson.build b/src/droid/meson.build
index bb7c46e..a308a91 100644
--- a/src/droid/meson.build
+++ b/src/droid/meson.build
@@ -1,4 +1,4 @@
-droid_sink = library('droid-sink',
+droid_sink = library('droid-sink' + droid_module_suffix,
   ['droid-sink.c', 'droid-sink.h'],
   dependencies : libdroid_util_dep,
   pic : true,
@@ -15,7 +15,7 @@ droid_sink_dep = declare_dependency(
   include_directories : [configinc],
 )
 
-droid_source = library('droid-source',
+droid_source = library('droid-source' + droid_module_suffix,
   ['droid-source.c', 'droid-source.h'],
   dependencies : libdroid_util_dep,
   pic : true,
@@ -31,30 +31,32 @@ droid_source_dep = declare_dependency(
   include_directories : [configinc],
 )
 
-module_sink = shared_module('module-droid-sink',
+droid_module_name_suffix = droid_module_suffix.underscorify()
+
+module_sink = shared_module('module-droid-sink' + droid_module_suffix,
   'module-droid-sink.c',
   name_prefix : '',
-  c_args : '-DPA_MODULE_NAME=module_droid_sink',
+  c_args : '-DPA_MODULE_NAME=module_droid_sink' + droid_module_name_suffix,
   dependencies : [droid_sink_dep, libdroid_util_dep],
   install : true,
   install_dir : modlibexecdir,
   install_rpath : rpath_dirs,
 )
 
-module_sink = shared_module('module-droid-source',
+module_sink = shared_module('module-droid-source' + droid_module_suffix,
   'module-droid-source.c',
   name_prefix : '',
-  c_args : '-DPA_MODULE_NAME=module_droid_source',
+  c_args : '-DPA_MODULE_NAME=module_droid_source' + droid_module_name_suffix,
   dependencies : [droid_source_dep, libdroid_util_dep],
   install : true,
   install_dir : modlibexecdir,
   install_rpath : rpath_dirs,
 )
 
-module_sink = shared_module('module-droid-card',
+module_sink = shared_module('module-droid-card' + droid_module_suffix,
   'module-droid-card.c',
   name_prefix : '',
-  c_args : '-DPA_MODULE_NAME=module_droid_card',
+  c_args : '-DPA_MODULE_NAME=module_droid_card' + droid_module_name_suffix,
   dependencies : [droid_sink_dep, droid_source_dep, libdroid_util_dep],
   install : true,
   install_dir : modlibexecdir,
